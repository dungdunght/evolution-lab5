/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collision;

import PetriCollisionEvent.PetriPetriCollisionEvent;
import PetriCollisionEvent.PetriPetriCollisionListener;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.BasicCollisionGroup;
import game.Game;
import game.PetriDish;
import java.awt.Point;
import java.util.ArrayList;

/**
 *  Class to check Petri-petri collision
 * @author dungdunght
 */
public class PetriPetriCollision extends BasicCollisionGroup {
    
        public PetriPetriCollision(Game g) {
        this.pixelPerfectCollision = true;
        owner = g;
       }
            /**
     * Check collision between Petri and Petri
     * @param sprite agar
     * @param sprite1 petri
     */
    @Override
    public void collided(Sprite sprite, Sprite sprite1) {
            
        int sizeSprite=((PetriDish)sprite).size();
        int sizeSprite1=((PetriDish)sprite1).size();
        
        if (sizeSprite1>sizeSprite){
            // remove petri lessly from game field.
            sprite.setActive(false);
            // make petri bigger

            ((PetriDish)sprite1).growUp(sizeSprite);
            fireLossEnermy(sprite);
            
        }
        else if (sizeSprite1<sizeSprite) {
            // remove petri lessly  from game field.
            sprite1.setActive(false);
            // make petri bigger

            ((PetriDish)sprite).growUp(sizeSprite1);
            fireLossEnermy(sprite1);
        }
        
        
       
    
    }
    //fire signal about petri was killed
     private ArrayList PetriPetriCollisisonListnerList = new ArrayList();
     public void addDivercantChangeCellListner(PetriPetriCollisionListener l) {
            PetriPetriCollisisonListnerList.add(l);
        }
      protected void fireLossEnermy(Sprite e) {
        for (Object listener : PetriPetriCollisisonListnerList)
        {
          
                  
            ((PetriPetriCollisionListener)listener).DieEnermy(e);
        }        
    }
    Game owner;
}
