package game;

import PetriCollisionEvent.PetriPetriCollisionListener;
import factory.DangerFactory;
import factory.AgarFactory;
import collision.*;
import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.object.*;
import com.golden.gamedev.object.background.*;
import java.awt.*;
import controller.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Game
 * @author anhcx
 */
public class Game extends GameObject {
    
    public Game(GameEngine gameEngine) {
        super(gameEngine); 
    }
  
    /**
     * Game play field. Where all event and object are scripted.
     */
    private PlayField playField;

    /**
     * Background. 
     */
    private Background bg;
    
    /** 
     * Group of sprite - petri.
     * This object can eat any agar and grow up.
     */
    SpriteGroup PETRI_GROUP;
    
    private AgarFactory agarFactory;
    
    /**
     * Group of sprite - agar.
     * Be generate each 3s. Agar is food of petri, make them grow up.
     */
    public SpriteGroup AGAR_GROUP;
    
    /**
     * Group of sprite - danger.
     */
    public SpriteGroup DANGER_GROUP;
    private DangerFactory dangerFactory;
    private int n;
    private int m;
    private a;
    private b;
    
    AgarPetriCollision apCollision;
    PetriDangerCollision pdCollision;
    PetriPetriCollision ppCollision;
    // All our actor
    private PetriDish player;
    private final java.util.List<PetriDish> computers = new ArrayList();
    
    PlayerControler controller;
    
    Timer agar_timer = new Timer(10000);
    
    private final java.util.List<PetriController> controllers = new ArrayList<>();
    
    @Override
    public void initResources() {
        // create play field.
        playField = new PlayField();
        
        // add background to play field.
        bg = new ImageBackground(getImage("resources/background.jpg"), TOTAL_WIDTH, TOTAL_HEIGHT);
        playField.setBackground(bg);      
        
        
        PETRI_GROUP = new SpriteGroup("PetriDish");      
        // create player with random on screen 640*480
        player = new PetriDish(getImage("resources/PRIMITIVE_PLANT.png"));
        Random rand=new Random();
        player.setPosition(new Point((rand.nextInt(400)), rand.nextInt(400)));
        controller = new PlayerControler(this, player);
        controllers.add(controller);
        PETRI_GROUP.add(player);
        
        // create ai
        for (int i = 0; i < NUMBER_ENEMY; i++) {
            PetriDish e = new PetriDish(getImage("resources/PRIMITIVE_ANIMAL.png"));
            computers.add(e);
            PETRI_GROUP.add(e);
            AIController ai = new AIController(this, player, e);
            controllers.add(ai);
        }
        
        AGAR_GROUP = new SpriteGroup("Agar");
        
        DANGER_GROUP = new SpriteGroup("Danger");
        
        dangerFactory = new DangerFactory(this);
        dangerFactory.generate();
        
        // generate agar
        agarFactory = new AgarFactory(this);
        agarFactory.generate();        
        
        
        // add all group to game field
        playField.addGroup(PETRI_GROUP);
        playField.addGroup(AGAR_GROUP);
        playField.addGroup(DANGER_GROUP);
        
        apCollision = new AgarPetriCollision(this);
        playField.addCollisionGroup(AGAR_GROUP, PETRI_GROUP, apCollision);
        
        pdCollision = new PetriDangerCollision(this);
        playField.addCollisionGroup(PETRI_GROUP, DANGER_GROUP, pdCollision);
        
        ppCollision = new PetriPetriCollision(this);
        ppCollision.addDivercantChangeCellListner(new AddEnermyObserver());
        playField.addCollisionGroup(PETRI_GROUP, PETRI_GROUP, ppCollision);
        // font
        font = fontManager.getFont(getImages("resources/font.png", 20, 3),
                                   " !            .,0123" +
                                   "456789:   -? ABCDEFG" +
                                   "HIJKLMNOPQRSTUVWXYZ ");
    }

    @Override
    public void update(long l) {
        
        playField.update(l);
        //bg.update(l);
        AGAR_GROUP.update(l);

        if (agar_timer.action(l)) {
            agarFactory.generate();
        }
        
        for (int i = 0; i < controllers.size(); i++) {
            controllers.get(i).update(l);
        }
       
  
        //playField.update(l);
        
    }
    
    
    @Override
    public void render(Graphics2D gd) {
        // render all characters
        playField.render(gd);  
        
        // render text
        font.drawString(gd, "PLAYER: " + String.valueOf(player.size()), 10, 10);
        
        for (int i = 0; i < computers.size(); i++) {
            font.drawString(gd, "AI " + String.valueOf(i+1) + ":" + String.valueOf(computers.get(i).size()), 10, 10+(i+1)*20);
        }
        
        // set center view to player
        bg.setToCenter(player);
         
        //playField.render(gd);   
    }
    
    /**
     * Get mouse position.
     * @return position
     */
    public Point mousePosition() {
        Point p = new Point(this.getMouseX(), this.getMouseY());
        p.x += bg.getX();
        p.y += bg.getY();
        return p;
    }
    
    public static int TOTAL_WIDTH = 6000; 
    
    public static int TOTAL_HEIGHT = 6000;
    
    public static int NUMBER_ENEMY = 10;
    
    public static int NUMBER_DANGER = 100;
    
    /**
     * Game fonts.
     */
    GameFont font;
   //receive signal about petri was killed 
   private class AddEnermyObserver implements PetriPetriCollisionListener{
       @Override
        public void DieEnermy(Sprite e) {
            
            if (e==player){
                parent.nextGameID = 1;
                finish();
            }
            computers.remove((PetriDish)e);
            PETRI_GROUP.remove(e);
            PetriDish e1 = new PetriDish(getImage("resources/PRIMITIVE_ANIMAL.png"));
            computers.add(e1);
            PETRI_GROUP.add(e1);
            AIController ai = new AIController(Game.this, player, e1);
            controllers.add(ai);
            //playField.update();
            
            }
   }
}


