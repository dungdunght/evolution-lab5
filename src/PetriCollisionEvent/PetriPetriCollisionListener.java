/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PetriCollisionEvent;

import com.golden.gamedev.object.Sprite;
import game.PetriDish;
import java.util.EventListener;

/**
 *  
 * @author dungdunght
 */
public interface PetriPetriCollisionListener extends EventListener{
    public void DieEnermy(Sprite e);
}
